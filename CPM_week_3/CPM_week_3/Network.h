//
//  Network.h
//  CPM_week_3
//
//  Created by Will Saults on 11/13/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import "Reachability.h"

#define kNetworkStatusChange @"kNetworkStatusChange"

@interface Network : Reachability
{

}

+(Network*)sharedNetwork;

- (BOOL) isConnected;

@end
