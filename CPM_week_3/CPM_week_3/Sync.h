//
//  Sync.h
//  CPM_week_3
//
//  Created by Will Saults on 11/14/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Celebrities;

@interface Sync : NSObject

+(Sync *)sharedSync;

- (void)createCelebrity:(Celebrities *)celebrity;
- (void)updateCelebrity:(Celebrities *)celebrity;
- (void)deleteCelebrity:(Celebrities *)celebrity;
- (void)downloadCelebrities;

@end
