//
//  Celebrities.h
//  CPM_week_3
//
//  Created by Will Saults on 11/13/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Celebrities : NSManagedObject

@property (nonatomic, retain) NSString * firstname;
@property (nonatomic, retain) NSString * lastname;
@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSDate * birthdate;
@property (nonatomic, retain) NSString * birth_place;
@property (nonatomic, retain) NSNumber * height_in_meters;
@property (nonatomic, retain) NSString * celeb_id;

@end
