//
//  Network.m
//  CPM_week_3
//
//  Created by Will Saults on 11/13/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import "Network.h"
#import "Reachability.h"

@interface Network()
{
    Reachability* hostReach;
    Reachability* internetReach;
    Reachability* wifiReach;
}

- (void) reachabilityChanged: (NSNotification* )note;

@end

@implementation Network

static Network * sharedNetwork = nil;

+(Network *)sharedNetwork{
    @synchronized(self){
        if (!sharedNetwork) {
            [Network new];
        }
    }
    return sharedNetwork;
}

+(id)alloc{
    @synchronized(self){
        if (!sharedNetwork)
            sharedNetwork = [super alloc];
    }
    return sharedNetwork;
}

- (id) init
{
    if (self=[super init]) 
    {
        // Observe the kNetworkReachabilityChangedNotification. When that notification is posted, the
        // method "reachabilityChanged" will be called.
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    
        //Change the host name here to change the server your monitoring
        hostReach = [Reachability reachabilityWithHostName: @"parse.com"];
        [hostReach startNotifier];
        internetReach = [Reachability reachabilityForInternetConnection];
        [internetReach startNotifier];
        wifiReach = [Reachability reachabilityForLocalWiFi];
        [wifiReach startNotifier];
    }
    return self;
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkStatusChange object:self];
}

- (BOOL) isConnected
{
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    if (netStatus == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cannot connect to database."
                                                        message:@"Please make sure you have an internet connection."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    
    return netStatus!=NotReachable;
}

@end
