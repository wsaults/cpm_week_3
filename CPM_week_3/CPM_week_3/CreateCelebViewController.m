//
//  CreateCelebViewController.m
//  CPM_week_3
//
//  Created by Will Saults on 11/14/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import "CreateCelebViewController.h"
#import "ModelManager.h" 
#import "Celebrities.h"

@interface CreateCelebViewController ()

@property (nonatomic, weak) IBOutlet UITextField *firstName;
@property (nonatomic, weak) IBOutlet UITextField *lastName;
@property (nonatomic, weak) IBOutlet UITextField *age;
@property (nonatomic, weak) IBOutlet UIButton *birthDate;
@property (nonatomic, weak) IBOutlet UITextField *height;
@property (nonatomic, weak) IBOutlet UITextField *celebId;
@property (nonatomic, weak) IBOutlet UITextField *birthPlace;
@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *submitButton;

@property (nonatomic) int mode;
typedef enum {
    CREATE_MODE = 0,
    UPDATE_MODE
} modeType;

@end

@implementation CreateCelebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_submitButton setEnabled:YES];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MMM-yy"];
    
    if (_celebrity == nil) {
        _mode = CREATE_MODE;
        [_birthDate setTitle:[dateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    } else {
        _mode = UPDATE_MODE;
        [_firstName setText:_celebrity.firstname];
        [_lastName setText:_celebrity.lastname];
        [_age setText:[_celebrity.age stringValue]];
        [_birthDate setTitle:[dateFormatter stringFromDate:_celebrity.birthdate] forState:UIControlStateNormal];
        [_datePicker setDate:_celebrity.birthdate];
        [_height setText:[_celebrity.height_in_meters stringValue]];
        [_birthPlace setText:_celebrity.birth_place];
        [_celebId setText:_celebrity.celeb_id];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    _celebrity = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)submit:(id)sender
{
    [_submitButton setEnabled:NO];
    if (_firstName.text.length == 0) {
        [self alertWithTitle:@"Missing first name" text:@"This field is required"];
        return;
    }
    
    if (_lastName.text.length == 0) {
        [self alertWithTitle:@"Missing last name" text:@"This field is required"];
        return;
    }
    
    if (_age.text.length == 0) {
        [self alertWithTitle:@"Missing age" text:@"This field is required"];
        return;
    }
    
    if (_height.text.length == 0) {
        [self alertWithTitle:@"Missing height" text:@"This field is required"];
        return;
    }
    
    if (_celebId.text.length == 0) {
        [self alertWithTitle:@"Missing ID" text:@"This field is required"];
        return;
    }
    
    if ([_birthDate.titleLabel.text isEqualToString:@"Birth date"]) {
        [self alertWithTitle:@"Missing Birth date" text:@"This field is required"];
        return;
    }
    
    int age = [_age.text intValue];
    float height = [_height.text floatValue];
    NSDate *date = [_datePicker date];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kDidSaveCelebrity object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [_submitButton setEnabled:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kDidSaveCelebrity object:nil];
    }];
    
    [[ModelManager sharedModelManager] updateCelebrityWithFirstName:_firstName.text
                                                           lastName:_lastName.text
                                                                age:age
                                                          birthdate:date
                                                        birth_place:_birthPlace.text
                                                   height_in_meters:height
                                                           celeb_id:_celebId.text];
}

- (void)alertWithTitle:(NSString *)title text:(NSString *)text
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:text
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil, nil];
    [alert show];
}

- (IBAction)dataPickerChanged:(id)sender
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd-MMM-yy"];
    [_birthDate setTitle:[dateFormatter stringFromDate:[_datePicker date]] forState:UIControlStateNormal];
}

- (IBAction)dismissKeyboard:(id)sender
{
    [_firstName resignFirstResponder];
    [_lastName resignFirstResponder];
    [_age resignFirstResponder];
    [_height resignFirstResponder];
    [_celebId resignFirstResponder];;
    [_birthPlace resignFirstResponder];
}


@end
