//
//  Celebrities.m
//  CPM_week_3
//
//  Created by Will Saults on 11/13/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import "Celebrities.h"


@implementation Celebrities

@dynamic firstname;
@dynamic lastname;
@dynamic age;
@dynamic birthdate;
@dynamic birth_place;
@dynamic height_in_meters;
@dynamic celeb_id;

@end
