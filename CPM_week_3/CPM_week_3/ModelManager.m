//
//  ModelManager.m
//  CPM_week_3
//
//  Created by Will Saults on 11/13/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import "ModelManager.h"
#import "Celebrities.h"
#import "Sync.h"
#import <Parse/Parse.h>

@implementation ModelManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static ModelManager *_sharedModelManager;

+(ModelManager *)sharedModelManager
{
    @synchronized([ModelManager class]) {
        if (!_sharedModelManager) {
            [self new];
        }
    }
    return _sharedModelManager;
}

+(id)alloc
{
    @synchronized([ModelManager class]) {
        NSAssert(_sharedModelManager == nil, @"Attempled second allocation of ModelManager singleton");
        _sharedModelManager = [super alloc];
    }
    return _sharedModelManager;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = [[ModelManager sharedModelManager] managedObjectContext];
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort(); // TODO: abort should be removed before shipping app.
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
         UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Local database does not match the current version of the app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
         */
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Entity creation
-(NSManagedObject *)createEntity:(NSString *)entityName
{
    return [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:[self managedObjectContext]];
}

#pragma mark - Entity deletion
-(void)deleteEntity:(NSManagedObject *)object
{
    [[self managedObjectContext] deleteObject:object];
    [[self managedObjectContext] processPendingChanges];
    [self saveContext];
}

#pragma mark - Entity fetch
-(NSArray *)getEntities:(NSString *)entityName
{
    NSManagedObjectContext *context = [[ModelManager sharedModelManager] managedObjectContext];
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:entityName];
    NSError *error;
    return [context executeFetchRequest:fetch error:&error];
}

-(NSArray *)getEntities:(NSString *)entityName
          withPredicate:(NSPredicate *)predicate
{
    NSManagedObjectContext *context = [[ModelManager sharedModelManager] managedObjectContext];
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [fetch setPredicate:predicate];
    NSError *error;
    return [context executeFetchRequest:fetch error:&error];
}

-(NSArray *)getEntities:(NSString *)entityName
            withSortKey:(NSString *)key
              ascending:(BOOL)ascending
          withPredicate:(NSPredicate *)predicate
{
    NSManagedObjectContext *context = [[ModelManager sharedModelManager] managedObjectContext];
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [fetch setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:key ascending:ascending];
    [fetch setSortDescriptors:@[sortDescriptor]];
    NSError *error;
    return [context executeFetchRequest:fetch error:&error];
}

-(NSArray *)getEntities:(NSString *)entityName
            withSortKey:(NSString *)key
              ascending:(BOOL)ascending
          withPredicate:(NSPredicate *)predicate
              batchSize:(NSUInteger)bSize
{
    NSManagedObjectContext *context = [[ModelManager sharedModelManager] managedObjectContext];
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [fetch setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:key ascending:NO];
    [fetch setSortDescriptors:@[sortDescriptor]];
    [fetch setFetchBatchSize:bSize];
    NSError *error;
    return [context executeFetchRequest:fetch error:&error];
}

-(NSFetchRequest *)getFetchRequestForEntity:(NSString *)entityName
                                withSortKey:(NSString *)key
                                  ascending:(BOOL)ascending
                              withPredicate:(NSPredicate *)predicate
                                  batchSize:(NSUInteger)bSize {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [fetchRequest setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:key ascending:NO];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    [fetchRequest setFetchBatchSize:bSize];
    [fetchRequest setFetchLimit:bSize];
    return fetchRequest;
}

#pragma mark - Entity count
-(int)countEntities:(NSString*)entity byPredicate:(NSPredicate*)predicate
{
    NSManagedObjectContext *context = [[ModelManager sharedModelManager] managedObjectContext];
    NSFetchRequest * fetch = [[NSFetchRequest alloc] initWithEntityName:entity];
    if (predicate!=nil) fetch.predicate = predicate;
    NSError * fetchError;
    return [context countForFetchRequest:fetch error:&fetchError];
}

#pragma mark - Entity creation conveniance methods
#pragma mark *** Celebrity ***
- (void)createCelebrityWithFirstName:(NSString *)first_name
                            lastName:(NSString *)last_name
                                 age:(int)age
                           birthdate:(NSDate *)birthdate
                         birth_place:(NSString *)birth_place
                    height_in_meters:(float)height_in_meters
                            celeb_id:(NSString *)celeb_id
{
    if ([self fetchCelebrityById:celeb_id] == nil) {
        // If the celebrity does not exist locally then add it to the local db.
        Celebrities *celebrity = (Celebrities *)[[ModelManager sharedModelManager] createEntity:kCelebrityEntityName];
        [celebrity setFirstname:first_name];
        [celebrity setLastname:last_name];
        [celebrity setAge:[NSNumber numberWithInt:age]];
        [celebrity setBirthdate:birthdate];
        [celebrity setBirth_place:birth_place];
        [celebrity setHeight_in_meters:[NSNumber numberWithFloat:height_in_meters]];
        [celebrity setCeleb_id:celeb_id];
        [self saveContext];
        NSLog(@"Celebrity created with id: %@", [celebrity celeb_id]);
        
        [[Sync sharedSync] createCelebrity:celebrity];
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidSaveCelebrity object:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cannot create celebrity."
                                                        message:@"That celebrity already exists in the database."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)updateCelebrityWithFirstName:(NSString *)first_name
                            lastName:(NSString *)last_name
                                 age:(int)age
                           birthdate:(NSDate *)birthdate
                         birth_place:(NSString *)birth_place
                    height_in_meters:(float)height_in_meters
                            celeb_id:(NSString *)celeb_id
{
    Celebrities *celebrity = [self fetchCelebrityById:celeb_id];
    if (celebrity != nil) {
        [celebrity setFirstname:first_name];
        [celebrity setLastname:last_name];
        [celebrity setAge:[NSNumber numberWithInt:age]];
        [celebrity setBirthdate:birthdate];
        [celebrity setBirth_place:birth_place];
        [celebrity setHeight_in_meters:[NSNumber numberWithFloat:height_in_meters]];
        [celebrity setCeleb_id:celeb_id];
        [self saveContext];
        
        [[Sync sharedSync] updateCelebrity:celebrity];
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidSaveCelebrity object:nil];
    } else {
        // If the celebrity does exist locally then add it to the local db.
        [self createCelebrityWithFirstName:first_name
                                  lastName:last_name
                                       age:age 
                                 birthdate:birthdate
                               birth_place:birth_place
                          height_in_meters:height_in_meters
                                  celeb_id:celeb_id];
    }
}

- (void)updateLocalCelebrities:(NSArray *)celebrities
{
    for (PFObject *celebrity in celebrities) {
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"dd-MMM-yy"];
        NSDate *date = [dateFormatter dateFromString:[celebrity objectForKey:@"birthdate"]];
        
        Celebrities *celeb = [self fetchCelebrityById:[celebrity objectForKey:@"celeb_id"]];
        if (celeb != nil) {
            [celeb setFirstname:[celebrity objectForKey:@"firstname"]];
            [celeb setLastname:[celebrity objectForKey:@"lastname"]];
            [celeb setAge:[celebrity objectForKey:@"age"]];
            [celeb setBirthdate:date];
            [celeb setBirth_place:[celebrity objectForKey:@"birth_place"]];
            [celeb setHeight_in_meters:[celebrity objectForKey:@"height_in_meters"]];
        } else {
            // If the celebrity does not exist locally then add it to the local db.
            Celebrities *newCeleb = (Celebrities *)[[ModelManager sharedModelManager] createEntity:kCelebrityEntityName];
            [newCeleb setFirstname:[celebrity objectForKey:@"firstname"]];
            [newCeleb setLastname:[celebrity objectForKey:@"lastname"]];
            [newCeleb setAge:[celebrity objectForKey:@"age"]];
            [newCeleb setBirthdate:date];
            [newCeleb setBirth_place:[celebrity objectForKey:@"birth_place"]];
            [newCeleb setHeight_in_meters:[celebrity objectForKey:@"height_in_meters"]];
            [newCeleb setCeleb_id:[celebrity objectForKey:@"celeb_id"]];
            NSLog(@"Celebrity created with id: %@", [newCeleb celeb_id]);
        }
    }
    
    [self saveContext];
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidDownloadCelebrities object:nil];
}

- (void)deleteCelebrityById:(NSString *)celeb_id
{
    Celebrities *celebrity = [self fetchCelebrityById:celeb_id];
    if (celebrity != nil) {
        [[Sync sharedSync] deleteCelebrity:celebrity];
        [self deleteEntity:celebrity];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cannot delete celebrity."
                                                        message:@"That celebrity does not exist in the database."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (Celebrities *)fetchCelebrityById:(NSString *)celeb_id
{
    NSArray *celebs = [self getEntities:kCelebrityEntityName withPredicate:[NSPredicate predicateWithFormat:@"celeb_id LIKE %@", celeb_id]];
    if (celebs.count > 0){
        return (Celebrities *)[celebs objectAtIndex:0];
    }
    return nil;
}

@end
