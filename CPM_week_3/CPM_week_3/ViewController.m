//
//  ViewController.m
//  CPM_week_3
//
//  Created by Will Saults on 11/13/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import "ViewController.h"
#import "ModelManager.h"
#import "Sync.h"
#import "Celebrities.h"
#import "CreateCelebViewController.h"

@interface ViewController ()

@property (nonatomic, strong) NSMutableArray *rows;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UITextField *ageTextField;
@property (nonatomic, weak) IBOutlet UITextField *idTextField;
@property (nonatomic) BOOL ascending;

@property (nonatomic) int selectedIndex;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _ascending = YES;
    
    // Download celebrities from DB
    [[Sync sharedSync] downloadCelebrities];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kDidDownloadCelebrities object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        // Reload the table when the download is done.
        _rows = [NSMutableArray arrayWithArray:[[ModelManager sharedModelManager] getEntities:kCelebrityEntityName]];
        [_tableView reloadData];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kDidDownloadCelebrities object:nil];
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _rows = [NSMutableArray arrayWithArray:[[ModelManager sharedModelManager] getEntities:kCelebrityEntityName]];
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tableview delegate methods
-(float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return  30.0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"First | Last | Age | B-Day | Location | Height";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (_rows != nil && _rows.count > 0) ? _rows.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    Celebrities *celebrity = (Celebrities *)[_rows objectAtIndex:indexPath.row];
    if (celebrity != nil) {
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"dd-MMM-yy"];
        NSString *text = [NSString stringWithFormat:@"%@ | %@ | %@ | %@ | %@ | %@", celebrity.firstname, celebrity.lastname, celebrity.age, [dateFormatter stringFromDate:celebrity.birthdate], celebrity.birth_place, celebrity.height_in_meters];
        cell.textLabel.text = text;
        [cell.textLabel setFont:[UIFont systemFontOfSize:10.0]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"ID: %@", celebrity.celeb_id];
        [cell.detailTextLabel setFont:[UIFont systemFontOfSize:10.0]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Segue to modal to update
    _selectedIndex = indexPath.row;
    [self performSegueWithIdentifier:@"celebModalSegue" sender:self];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView beginUpdates];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        NSString *celeb_id = cell.detailTextLabel.text;
        if (celeb_id.length > 0) {
            // Do whatever data deletion you need to do...
            [[ModelManager sharedModelManager] deleteCelebrityById:[celeb_id stringByReplacingOccurrencesOfString:@"ID: " withString:@""]];
            
            // Delete the row from the data source
            if (indexPath.row <= _rows.count) {
                [_rows removeObjectAtIndex:indexPath.row];
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationTop];
            }
        } else {
            // Error! No ID
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error - commitEditingStyle."
                                                            message:@"Issue reading ID."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    [tableView endUpdates];
}

#pragma mark - Text field delegate method
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length > 0) {
        [self queryWithText:[NSString stringWithFormat:@"%@%@", textField.text, string] tag:textField.tag];
    }
    return YES;
}

- (void)queryWithText:(NSString *)text tag:(int)tag;
{
    NSPredicate *predicate = [NSPredicate new];
    switch (tag) {
        case 0:
            predicate = [NSPredicate predicateWithFormat:@"age = %d", [text intValue]];
            break;
            
        case 1:
            predicate = [NSPredicate predicateWithFormat:@"celeb_id CONTAINS[cd] %@", text];
            break;
            
            
        default:
            break;
    }
    
    _rows = [NSMutableArray arrayWithArray:[[ModelManager sharedModelManager] getEntities:kCelebrityEntityName withPredicate:predicate]];
    [_tableView reloadData];
}

- (IBAction)sortByLastName:(id)sender
{
    _ascending = (_ascending == YES) ? NO : YES;
    _rows = [NSMutableArray arrayWithArray:[[ModelManager sharedModelManager] getEntities:kCelebrityEntityName
                                                                              withSortKey:@"lastname"
                                                                                ascending:_ascending
                                                                            withPredicate:nil]];
    [_tableView reloadData];
}

- (IBAction)sortByHeight:(id)sender
{
    _ascending = (_ascending == YES) ? NO : YES;
    _rows = [NSMutableArray arrayWithArray:[[ModelManager sharedModelManager] getEntities:kCelebrityEntityName
                                                                              withSortKey:@"height_in_meters"
                                                                                ascending:_ascending
                                                                            withPredicate:nil]];
    [_tableView reloadData];
}

- (IBAction)dismissKeyboard:(id)sender
{
    [_ageTextField setText:@""];
    [_idTextField setText:@""];
    [_ageTextField resignFirstResponder];
    [_idTextField resignFirstResponder];
    [self reset];
}

- (void)reset
{
    _rows = [NSMutableArray arrayWithArray:[[ModelManager sharedModelManager] getEntities:kCelebrityEntityName]];
    [_tableView reloadData];
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"celebModalSegue"]) {
        if (_selectedIndex <= _rows.count ) {
            Celebrities *celebrity = (Celebrities *)[_rows objectAtIndex:_selectedIndex];
            if (celebrity != nil) {
                CreateCelebViewController *ccvc = (CreateCelebViewController *)[segue destinationViewController];
                [ccvc setCelebrity:celebrity];
            }
        }
    }
}


@end
