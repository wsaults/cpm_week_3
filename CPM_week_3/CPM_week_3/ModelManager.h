//
//  ModelManager.h
//  CPM_week_3
//
//  Created by Will Saults on 11/13/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#pragma mark - Model entity names
#define kCelebrityEntityName @"Celebrities"
#define kDidDownloadCelebrities @"didDownloadCelebrities"
#define kDidSaveCelebrity @"didSaveCelebrity"

@class Celebrities;

@interface ModelManager : NSObject {
    
}

+(ModelManager *)sharedModelManager;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

#pragma mark - Entity creation
-(NSManagedObject *)createEntity:(NSString *)entityName;

#pragma mark - Entity deletion
- (void)deleteEntity:(NSManagedObject *)object;

#pragma mark - Entity fetch
- (NSArray *)getEntities:(NSString *)entityName;
- (NSArray *)getEntities:(NSString *)entityName withPredicate:(NSPredicate *)predicate;
- (NSArray *)getEntities:(NSString *)entityName withSortKey:(NSString *)key ascending:(BOOL)ascending withPredicate:(NSPredicate *)predicate;
- (NSArray *)getEntities:(NSString *)entityName withSortKey:(NSString *)key ascending:(BOOL)ascending withPredicate:(NSPredicate *)predicate batchSize:(NSUInteger)bSize;
- (NSFetchRequest *)getFetchRequestForEntity:(NSString *)entityName withSortKey:(NSString *)key ascending:(BOOL)ascending withPredicate:(NSPredicate *)predicate batchSize:(NSUInteger)bSize;

#pragma mark - Entity count
- (int)countEntities:(NSString*)entity byPredicate:(NSPredicate*)predicate;

#pragma mark - Entity creation conveniance methods
#pragma mark *** Celebrity ***
- (void)createCelebrityWithFirstName:(NSString *)first_name
                            lastName:(NSString *)last_name
                                 age:(int)age
                           birthdate:(NSDate *)birthdate
                         birth_place:(NSString *)birth_place
                    height_in_meters:(float)height_in_meters
                            celeb_id:(NSString *)celeb_id;

- (void)updateCelebrityWithFirstName:(NSString *)first_name
                            lastName:(NSString *)last_name
                                 age:(int)age
                           birthdate:(NSDate *)birthdate
                         birth_place:(NSString *)birth_place
                    height_in_meters:(float)height_in_meters
                            celeb_id:(NSString *)celeb_id;

- (void)updateLocalCelebrities:(NSArray *)celebrities;

- (void)deleteCelebrityById:(NSString *)celeb_id;

- (Celebrities *)fetchCelebrityById:(NSString *)celeb_id;

@end
