//
//  Sync.m
//  CPM_week_3
//
//  Created by Will Saults on 11/14/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import "Sync.h"
#import <Parse/Parse.h>
#import "Celebrities.h"
#import "ModelManager.h"
#import "Network.h"

@implementation Sync

static Sync *_sharedSync;

+(Sync *)sharedSync
{
    @synchronized([Sync class]) {
        if (!_sharedSync) {
            [self new];
        }
    }
    return _sharedSync;
}

+(id)alloc
{
    @synchronized([Sync class]) {
        NSAssert(_sharedSync == nil, @"Attempled second allocation of Sync singleton");
        _sharedSync = [super alloc];
    }
    return _sharedSync;
}

- (void)createCelebrity:(Celebrities *)celebrity
{
    if (celebrity != nil && [[Network sharedNetwork] isConnected]) {
        PFQuery *query = [PFQuery queryWithClassName:kCelebrityEntityName];
        [query whereKey:@"celeb_id" equalTo:celebrity.celeb_id];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded.
                NSLog(@"Success");
                // If the record exits in the remote db then dont do anything.
                if (objects.count == 0) {
                    NSDateFormatter *dateFormatter = [NSDateFormatter new];
                    [dateFormatter setDateFormat:@"dd-MMM-yy"];
                    NSString *date = [dateFormatter stringFromDate:celebrity.birthdate];
                    PFObject *celebObject = [PFObject objectWithClassName:kCelebrityEntityName];
                    [celebObject setObject:celebrity.firstname forKey:@"firstname"];
                    [celebObject setObject:celebrity.lastname forKey:@"lastname"];
                    [celebObject setObject:celebrity.age forKey:@"age"];
                    [celebObject setObject:date forKey:@"birthdate"];
                    [celebObject setObject:celebrity.birth_place forKey:@"birth_place"];
                    [celebObject setObject:celebrity.height_in_meters forKey:@"height_in_meters"];
                    [celebObject setObject:celebrity.celeb_id forKey:@"celeb_id"];
                    [celebObject saveInBackground];
                }
            } else {
                // Log details of the failure
                NSLog(@"Error - createCelebrity: %@ %@", error, [error userInfo]);
            }
        }];
    }
}

- (void)updateCelebrity:(Celebrities *)celebrity
{
    if (celebrity != nil && [[Network sharedNetwork] isConnected]) {
        PFQuery *query = [PFQuery queryWithClassName:kCelebrityEntityName];
        [query whereKey:@"celeb_id" equalTo:celebrity.celeb_id];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded.
                NSLog(@"Success");
                // Do something with the found objects
                for (PFObject *celebObject in objects) {
                    NSLog(@"%@", celebObject.objectId);
                    NSDateFormatter *dateFormatter = [NSDateFormatter new];
                    [dateFormatter setDateFormat:@"dd-MMM-yy"];
                    NSString *date = [dateFormatter stringFromDate:celebrity.birthdate];
                    [celebObject setObject:celebrity.firstname forKey:@"firstname"];
                    [celebObject setObject:celebrity.lastname forKey:@"lastname"];
                    [celebObject setObject:celebrity.age forKey:@"age"];
                    [celebObject setObject:date forKey:@"birthdate"];
                    [celebObject setObject:celebrity.birth_place forKey:@"birth_place"];
                    [celebObject setObject:celebrity.height_in_meters forKey:@"height_in_meters"];
                    [celebObject setObject:celebrity.celeb_id forKey:@"celeb_id"];
                    [celebObject saveInBackground];
                }
            } else {
                // Log details of the failure
                NSLog(@"Error - updateCelebrity: %@ %@", error, [error userInfo]);
            }
        }];
    }
}

- (void)deleteCelebrity:(Celebrities *)celebrity
{
    // Create a local version of the celebrity.
    Celebrities *celeb = celebrity;
    if (celeb != nil && [[Network sharedNetwork] isConnected]) {
        PFQuery *query = [PFQuery queryWithClassName:kCelebrityEntityName];
        [query whereKey:@"celeb_id" equalTo:celeb.celeb_id];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded.
                NSLog(@"Success");
                // Do something with the found objects
                for (PFObject *celebObject in objects) {
                    NSLog(@"%@", celebObject.objectId);
                    [celebObject deleteInBackground];
                }
            } else {
                // Log details of the failure
                NSLog(@"Error - deleteCelebrity: %@ %@", error, [error userInfo]);
            }
        }];
    }
}

- (void)downloadCelebrities
{
    if (![[Network sharedNetwork] isConnected]) return;
    PFQuery *query = [PFQuery queryWithClassName:kCelebrityEntityName];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Success");
            // Do something with the found objects
            if (objects.count > 0) {
                [[ModelManager sharedModelManager] updateLocalCelebrities:objects];
            }
        } else {
            // Log details of the failure
            NSLog(@"Error - downloadCelebrities: %@ %@", error, [error userInfo]);
        }
    }];
}

@end
