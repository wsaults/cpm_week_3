//
//  CreateCelebViewController.h
//  CPM_week_3
//
//  Created by Will Saults on 11/14/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Celebrities;

@interface CreateCelebViewController : UIViewController

@property (nonatomic, strong) Celebrities *celebrity;

@end
