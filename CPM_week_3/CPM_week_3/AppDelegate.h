//
//  AppDelegate.h
//  CPM_week_3
//
//  Created by Will Saults on 11/13/13.
//  Copyright (c) 2013 Fullsail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
